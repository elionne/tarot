use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

fn event_from_file(path: &str) -> Result<tarot::Event, Box<dyn Error>> {
    let file = File::open(Path::new("tests/fixtures").join(path))?;
    let reader = BufReader::new(file);
    let event = serde_yaml::from_reader(reader)?;

    Ok(event)
}


fn find_player<'a>(players: &'a Vec<tarot::PlayerPoints>, name: &str) -> Option<&'a tarot::PlayerPoints> {
    players.iter().find(|player| &player.name == name)
}


#[test]
fn points1() {
    let event: tarot::Event = event_from_file("points1.yml").unwrap();

    assert_eq!(4, event.players.len());
    assert_eq!(1, event.games.len());

    let points = tarot::collect_points(&event);
    let last = points.last().unwrap();

    // Moi  | toi  | lui  | elle
    //  318 | -106 | -106 | -106
    assert_eq!(318, find_player(last, "Moi").unwrap().points);
    assert_eq!(-106, find_player(last, "toi").unwrap().points);
    assert_eq!(-106, find_player(last, "lui").unwrap().points);
    assert_eq!(-106, find_player(last, "elle").unwrap().points);
}


#[test]
fn points2() {
    let event: tarot::Event = event_from_file("points2.yml").unwrap();

    assert_eq!(4, event.players.len());
    assert_eq!(1, event.games.len());

    let points = tarot::collect_points(&event);
    let last = points.last().unwrap();
    // Moi  | toi  | lui  | elle
    //  228 |  -76 |  -76 |  -76

    assert_eq!(228, find_player(last, "Moi").unwrap().points);
    assert_eq!(-76, find_player(last, "toi").unwrap().points);
    assert_eq!(-76, find_player(last, "lui").unwrap().points);
    assert_eq!(-76, find_player(last, "elle").unwrap().points);
}


#[test]
fn points3() {
    let event: tarot::Event = event_from_file("points3.yml").unwrap();

    assert_eq!(4, event.players.len());
    assert_eq!(1, event.games.len());

    let points = tarot::collect_points(&event);
    let last = points.last().unwrap();

    // Moi  | toi  | lui  | elle
    // -126 |   42 |   42 |   42
    assert_eq!(-126, find_player(last, "Moi").unwrap().points);
    assert_eq!(42, find_player(last, "toi").unwrap().points);
    assert_eq!(42, find_player(last, "lui").unwrap().points);
    assert_eq!(42, find_player(last, "elle").unwrap().points);
}


#[test]
fn points4() {
    let event: tarot::Event = event_from_file("points4.yml").unwrap();

    assert_eq!(4, event.players.len());
    assert_eq!(1, event.games.len());

    let points = tarot::collect_points(&event);
    let last = points.last().unwrap();

    // Moi  | toi  | lui  | elle
    //  276 |  -92 |  -92 |  -92

    assert_eq!(276, find_player(last, "Moi").unwrap().points);
    assert_eq!(-92, find_player(last, "toi").unwrap().points);
    assert_eq!(-92, find_player(last, "lui").unwrap().points);
    assert_eq!(-92, find_player(last, "elle").unwrap().points);
}

#[test]
fn points5() {
    let event: tarot::Event = event_from_file("points5.yml").unwrap();

    assert_eq!(4, event.players.len());
    assert_eq!(1, event.games.len());

    let points = tarot::collect_points(&event);
    let last = points.last().unwrap();

    // Moi  | toi  | lui  | elle
    // 1746 | -582 | -582 | -582

    assert_eq!(1746, find_player(last, "Moi").unwrap().points);
    assert_eq!(-582, find_player(last, "toi").unwrap().points);
    assert_eq!(-582, find_player(last, "lui").unwrap().points);
    assert_eq!(-582, find_player(last, "elle").unwrap().points);
}
